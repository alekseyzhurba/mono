package dao.entity;

import dao.UserDAO;
import dao.connection.DAOConnectionPool;
import entity.User;
import org.junit.AfterClass;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.List;

import static org.hamcrest.CoreMatchers.hasItems;


public class UserDAOTest {

    private UserDAO userDAO = new UserDAO();

    @Before
    public void init() throws SQLException {
        String SQL = "truncate table app_user cascade";
        Connection connection = DAOConnectionPool.getInstance().getConnection();
        PreparedStatement statement = connection.prepareStatement(SQL);
        statement.executeUpdate();
        DAOConnectionPool.getInstance().releaseConnection(connection);
    }


    @Test
    public void testSaveUser() {
        User user = new User();
        user.setPhone("0965563505");
        user.setEmail("torba@gmail.com");
        user.setFullName("Hovrashok Torba");

        userDAO.save(user);

        List<User> users = userDAO.findAll();
        Assert.assertEquals(user, users.get(0));
    }

    @Test
    public void testGetLoginByCard() {
        User user = new User();
        user.setPhone("0965563505");
        user.setEmail("doe@gmail.com");
        user.setFullName("John Doe");

        userDAO.save(user);

        String login = userDAO.getLoginByCard(user.getFullName());
        Assert.assertEquals(user.getPhone(), login);
    }

    @Test
    public void testGetAll() {
        User user = new User();
        user.setPhone("0965563505");
        user.setEmail("doe@gmail.com");
        user.setFullName("John Doe");

        userDAO.save(user);

        User user1 = new User();
        user1.setPhone("dragon");
        user1.setEmail("snow@gmail.com");
        user1.setFullName("John Snow");

        userDAO.save(user1);

        User user2 = new User();
        user2.setPhone("lord");
        user2.setEmail("golum@gmail.com");
        user2.setFullName("Golum");

        userDAO.save(user2);

        List<User> savedUsers = userDAO.findAll();

        Assert.assertEquals(3,savedUsers.size() );
        Assert.assertThat(savedUsers, hasItems(user, user1, user2));
    }

    @AfterClass
    public static void endOfTests() throws SQLException {
        String SQL = "truncate table app_user cascade";
        Connection connection = DAOConnectionPool.getInstance().getConnection();
        PreparedStatement statement = connection.prepareStatement(SQL);
        statement.executeUpdate();
        DAOConnectionPool.getInstance().releaseConnection(connection);
        DAOConnectionPool.getInstance().destroyConnections();
    }
}