package dao.entity;

import dao.MovieSessionDAO;
import dao.connection.DAOConnectionPool;
import entity.Cinema;
import entity.Movie;
import entity.MovieSession;
import entity.User;
import org.junit.AfterClass;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.time.LocalDateTime;
import java.util.List;

import static org.hamcrest.CoreMatchers.hasItems;


public class MovieSessionDAOTest {

    MovieSessionDAO movieSessionDAO = new MovieSessionDAO();

    @Before
    public void init() throws SQLException {
        String SQL = "truncate table movie_session, movie, cinema cascade;" +
                "insert into movie (id, title) values(12, 'Terminator 2');" +
                "insert into cinema (id, name, row_count, seats_in_row) values(23, 'Dafi', 13, 10 )";
        Connection connection = DAOConnectionPool.getInstance().getConnection();
        PreparedStatement statement = connection.prepareStatement(SQL);
        statement.executeUpdate();
        DAOConnectionPool.getInstance().releaseConnection(connection);
    }

    @Test
    public void testSave() {
        MovieSession movieSession = new MovieSession();
        movieSession.setId(1L);
        movieSession.setSessionTime(LocalDateTime.now());
        movieSession.setPrice(80.0);

        Movie movie = new Movie();
        movie.setId(12l);

        Cinema cinema = new Cinema();
        cinema.setId(23L);

        movieSession.setMovie(movie);
        movieSession.setCinema(cinema);

        movieSessionDAO.save(movieSession);

        List<MovieSession> sessions = movieSessionDAO.findAll();
        Assert.assertEquals(movieSession, sessions.get(0));
    }

    @Test
    public void testGetAllMovieSessions() {
        MovieSession movieSession = new MovieSession();
        movieSession.setId(1L);
        movieSession.setSessionTime(LocalDateTime.now());
        movieSession.setPrice(80.0);

        Movie movie = new Movie();
        movie.setId(12l);

        Cinema cinema = new Cinema();
        cinema.setId(23L);

        movieSession.setMovie(movie);
        movieSession.setCinema(cinema);
        movieSessionDAO.save(movieSession);

        MovieSession movieSession1 = new MovieSession();
        movieSession1.setId(2L);
        movieSession1.setSessionTime(LocalDateTime.now());
        movieSession1.setPrice(145.0);
        movieSession1.setMovie(movie);
        movieSession1.setCinema(cinema);
        movieSessionDAO.save(movieSession1);


        MovieSession movieSession2 = new MovieSession();
        movieSession2.setId(3L);
        movieSession2.setSessionTime(LocalDateTime.now());
        movieSession2.setPrice(180.0);
        movieSession2.setMovie(movie);
        movieSession2.setCinema(cinema);
        movieSessionDAO.save(movieSession2);

        List<MovieSession> savedSessions = movieSessionDAO.findAll();

        Assert.assertEquals(3, savedSessions.size());
        Assert.assertThat(savedSessions, hasItems(movieSession, movieSession1, movieSession2));
    }

    @AfterClass
    public static void endOfTests() throws SQLException {
        String SQL = "truncate table movie_session cascade";
        Connection connection = DAOConnectionPool.getInstance().getConnection();
        PreparedStatement statement = connection.prepareStatement(SQL);
        statement.executeUpdate();
        DAOConnectionPool.getInstance().releaseConnection(connection);
        DAOConnectionPool.getInstance().destroyConnections();
    }
}