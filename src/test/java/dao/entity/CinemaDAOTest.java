package dao.entity;

import dao.CinemaDAO;
import dao.connection.DAOConnectionPool;
import entity.Cinema;
import org.junit.AfterClass;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.List;

import static org.hamcrest.CoreMatchers.hasItems;


public class CinemaDAOTest {

    private CinemaDAO cinemaDAO = new CinemaDAO();

    @Before
    public void init() throws SQLException {
        String SQL = "truncate table cinema cascade";
        Connection connection = DAOConnectionPool.getInstance().getConnection();
        PreparedStatement statement = connection.prepareStatement(SQL);
        statement.executeUpdate();
        DAOConnectionPool.getInstance().releaseConnection(connection);
    }

    @Test
    public void testSaveCinema() {
        Cinema cinema = new Cinema();
        cinema.setId(1L);
        cinema.setName("Most-kino");
        cinema.setRowCount((byte) 15);
        cinema.setSeatsInRow((byte) 12);

        cinemaDAO.save(cinema);

        List<Cinema> cinemas = cinemaDAO.findAll();
        Assert.assertEquals(cinema, cinemas.get(0));
    }

    @Test
    public void testGetAllCinemas() {
        Cinema cinema = new Cinema();
        cinema.setId(1L);
        cinema.setName("Most-kino");
        cinema.setRowCount((byte) 15);
        cinema.setSeatsInRow((byte) 12);

        cinemaDAO.save(cinema);

        Cinema cinema1 = new Cinema();
        cinema1.setId(2L);
        cinema1.setName("Dafi");
        cinema1.setRowCount((byte) 12);
        cinema1.setSeatsInRow((byte) 15);

        cinemaDAO.save(cinema1);

        Cinema cinema2 = new Cinema();
        cinema2.setId(3L);
        cinema2.setName("Kino-kino");
        cinema2.setRowCount((byte) 15);
        cinema2.setSeatsInRow((byte) 12);

        cinemaDAO.save(cinema2);

        List<Cinema> cinemaList = cinemaDAO.findAll();

        Assert.assertEquals(3,cinemaList.size() );
        Assert.assertThat(cinemaList, hasItems(cinema, cinema1, cinema2));
    }

    @AfterClass
    public static void endOfTests() throws SQLException {
        String SQL = "truncate table cinema cascade";
        Connection connection = DAOConnectionPool.getInstance().getConnection();
        PreparedStatement statement = connection.prepareStatement(SQL);
        statement.executeUpdate();
        DAOConnectionPool.getInstance().releaseConnection(connection);
        DAOConnectionPool.getInstance().destroyConnections();
    }
}