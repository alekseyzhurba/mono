package dao.entity;

import dao.PaymentDAO;
import dao.connection.DAOConnectionPool;
import entity.Payment;
import entity.enumeration.PaymentStatus;
import org.junit.AfterClass;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.List;
import java.util.UUID;


public class PaymentDAOTest {

    private PaymentDAO paymentDAO = new PaymentDAO();

    @Before
    public void init() throws SQLException {
        String SQL = "truncate table payment, app_user, movie, cinema, movie_session, ticket cascade;" +
                "call insert_test_data()";
        Connection connection = DAOConnectionPool.getInstance().getConnection();
        PreparedStatement statement = connection.prepareStatement(SQL);
        statement.executeUpdate();
        DAOConnectionPool.getInstance().releaseConnection(connection);
    }

    @Test
    public void testSavePayment() {
        Payment payment = new Payment();
        payment.setPaymentCode(UUID.randomUUID());
        payment.setAmount(80.5);
        payment.setCardNum("Petro Petrov");
        payment.setStatus(PaymentStatus.NEW);

        paymentDAO.save(payment);

        List<Payment> payments = paymentDAO.findAll();

        Assert.assertEquals(payment, payments.get(0));
    }

    @Test
    public void testUpdatePayment() {
        Payment payment = new Payment();
        payment.setPaymentCode(UUID.randomUUID());
        payment.setAmount(80.5);
        payment.setCardNum("Petro Petrov");
        payment.setStatus(PaymentStatus.NEW);

        paymentDAO.save(payment);

        payment.setStatus(PaymentStatus.NOT_PASSED);

        paymentDAO.update(payment);

        List<Payment> payments = paymentDAO.findAll();

        Assert.assertEquals(payment.getStatus(), payments.get(0).getStatus());
    }




    @AfterClass
    public static void endOfTests() throws SQLException {
        String SQL = "truncate table payment, app_user, movie, cinema, movie_session, ticket cascade;";
        Connection connection = DAOConnectionPool.getInstance().getConnection();
        PreparedStatement statement = connection.prepareStatement(SQL);
        statement.executeUpdate();
        DAOConnectionPool.getInstance().releaseConnection(connection);
        DAOConnectionPool.getInstance().destroyConnections();
    }
}