package dao.entity;

import dao.MovieDAO;
import dao.connection.DAOConnectionPool;
import entity.Movie;
import org.junit.AfterClass;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.List;

import static org.hamcrest.CoreMatchers.hasItems;


public class MovieDAOTest {

    private MovieDAO movieDAO = new MovieDAO();

    @Before
    public void init() throws SQLException {
        String SQL = "truncate table movie cascade";
        Connection connection = DAOConnectionPool.getInstance().getConnection();
        PreparedStatement statement = connection.prepareStatement(SQL);
        statement.executeUpdate();
        DAOConnectionPool.getInstance().releaseConnection(connection);
    }

    @Test
    public void testSaveMovie() {
        Movie movie = new Movie();
        movie.setId(1L);
        movie.setTitle("Jakass");

        movieDAO.save(movie);

        List<Movie> movies = movieDAO.findAll();
        Assert.assertEquals(movie, movies.get(0));
    }

    @Test
    public void testGetAllMovies() {
        Movie movie = new Movie();
        movie.setId(1L);
        movie.setTitle("Jakass");

        movieDAO.save(movie);

        Movie movie1 = new Movie();
        movie1.setId(2L);
        movie1.setTitle("Space Jam");

        movieDAO.save(movie1);

        Movie movie2 = new Movie();
        movie2.setId(3L);
        movie2.setTitle("How High");

        movieDAO.save(movie2);

        List<Movie> savedMovie = movieDAO.findAll();

        Assert.assertEquals(3, savedMovie.size());
        Assert.assertThat(savedMovie, hasItems(movie, movie1, movie2));
    }

    @AfterClass
    public static void endOfTests() throws SQLException {
        String SQL = "truncate table movie cascade";
        Connection connection = DAOConnectionPool.getInstance().getConnection();
        PreparedStatement statement = connection.prepareStatement(SQL);
        statement.executeUpdate();
        DAOConnectionPool.getInstance().releaseConnection(connection);
        DAOConnectionPool.getInstance().destroyConnections();
    }
}