package dao;


import dao.connection.DAOConnectionPool;
import entity.Cinema;
import exception.DAOException;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Logger;

public class CinemaDAO implements BaseDAO<Cinema, Long> {

    private final Logger log = Logger.getLogger(this.getClass().getName());

    private static final String SQL_SAVE = "INSERT INTO cinema VALUES (?,?,?,?)";
    private static final String SQL_GET_ALL = "SELECT * FROM cinema";


    @Override
    public List<Cinema> findAll() throws DAOException{
        log.info("Searching all in table cinema");
        log.info("Opening a database connection");
        Connection connection = DAOConnectionPool.getInstance().getConnection();
        List<Cinema> cinemaList = new ArrayList<>();
        try (PreparedStatement statement = connection.prepareStatement(SQL_GET_ALL);
             ResultSet resultSet = statement.executeQuery()) {
            log.info("Connection established");
            while (resultSet.next()) {
                Cinema cinema = new Cinema();
                cinema.setId(resultSet.getLong("id"));
                cinema.setName(resultSet.getString("name"));
                cinema.setRowCount(resultSet.getByte("row_count"));
                cinema.setSeatsInRow(resultSet.getByte("seats_in_row"));

                cinemaList.add(cinema);
            }
        } catch (SQLException e) {
            throw new DAOException("Failed connection " + e.getMessage());
        } finally {
            if (connection != null) {
                DAOConnectionPool.getInstance().releaseConnection(connection);
            }
        }
        log.info("All cinemas found");
        return cinemaList;
    }

    @Override
    public void save(Cinema cinema) throws DAOException{
        log.info("Saving cinema");
        log.info("Opening a database connection for saving cinema");
        Connection connection = DAOConnectionPool.getInstance().getConnection();
        try (PreparedStatement statement = connection.prepareStatement(SQL_SAVE)) {
            log.info("Connection established");

            log.info("Create prepared statement for saving cinema");
            statement.setLong(1, cinema.getId());
            statement.setString(2, cinema.getName());
            statement.setInt(3, cinema.getRowCount());
            statement.setInt(4, cinema.getSeatsInRow());

            statement.executeUpdate();
            log.info("Statement created");

        } catch (SQLException e) {
            throw new DAOException("Cannot connect to the database", e);
        } finally {
            if (connection != null) {
                DAOConnectionPool.getInstance().releaseConnection(connection);
            }
        }
        log.info("Cinema saved successfully");
    }

    @Override
     public void printAll() throws DAOException {
        log.info("Searching all cinema to print");
        log.info("Opening a database connection");
        Connection connection = DAOConnectionPool.getInstance().getConnection();
        try (PreparedStatement statement = connection.prepareStatement(SQL_GET_ALL);
             ResultSet resultSet = statement.executeQuery()) {
            log.info("Connection established");
            ResultSetMetaData rsmd = resultSet.getMetaData();

            int line = 1;
            System.out.printf("TABLE %s\n", "CINEMA");
            System.out.printf("%c,\t%s\t, %s\t, %s, %s\n", '#',
                    rsmd.getColumnName(1),
                    rsmd.getColumnName(2),
                    rsmd.getColumnName(3),
                    rsmd.getColumnName(4));
            while (resultSet.next()) {
                System.out.printf("%d, %d, %s\t\t, %d\t, %d\n",
                        line++, resultSet.getLong(1), resultSet.getString(2),
                        resultSet.getInt(3), resultSet.getInt(4));
            }
        } catch (SQLException e) {
            throw new DAOException("Failed connection " + e.getMessage());
        } finally {
            DAOConnectionPool.getInstance().releaseConnection(connection);
        }
    }
}
