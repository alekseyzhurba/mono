package dao;


import dao.connection.DAOConnectionPool;
import entity.Movie;
import exception.DAOException;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Logger;

public class MovieDAO implements BaseDAO<Movie, Long> {

    private final Logger log = Logger.getLogger(this.getClass().getName());

    private static final String SQL_GET_ALL = "SELECT * FROM movie";
    private static final String SQL_SAVE = "INSERT INTO movie VALUES (?,?)";

    @Override
    public List<Movie> findAll() throws DAOException {
        log.info("Searching all in table movie");
        log.info("Opening a database connection");
        Connection connection = DAOConnectionPool.getInstance().getConnection();
        List<Movie> movies = new ArrayList<>();
        try (PreparedStatement statement = connection.prepareStatement(SQL_GET_ALL);
             ResultSet resultSet = statement.executeQuery()) {
            log.info("Connection established");
            while (resultSet.next()) {
                Movie movie = new Movie();
                movie.setId(resultSet.getLong("id"));
                movie.setTitle(resultSet.getString("title"));

                movies.add(movie);
            }
        } catch (SQLException e) {
            throw new DAOException("Failed connection " + e.getMessage());
        } finally {
            if (connection != null) {
                DAOConnectionPool.getInstance().releaseConnection(connection);
            }
        }
        log.info("Movies found");
        return movies;
    }

    @Override
    public void save(Movie entity) throws DAOException {
        log.info("Saving movie");
        log.info("Opening a database connection for saving movie");
        Connection connection = DAOConnectionPool.getInstance().getConnection();
        try (PreparedStatement statement = connection.prepareStatement(SQL_SAVE)) {
            log.info("Connection established");

            statement.setLong(1, entity.getId());
            statement.setString(2, entity.getTitle());
            statement.executeUpdate();

        } catch (SQLException e) {
            throw new DAOException("Cannot connect to the database", e);
        } finally {
            DAOConnectionPool.getInstance().releaseConnection(connection);
        }
        log.info("Movie saved successfully");
    }

    @Override
    public void printAll() throws DAOException {
        log.info("Searching all cinema to print");
        log.info("Opening a database connection");
        Connection connection = DAOConnectionPool.getInstance().getConnection();
        try (PreparedStatement statement = connection.prepareStatement(SQL_GET_ALL);
             ResultSet resultSet = statement.executeQuery()) {
            log.info("Connection established");
            ResultSetMetaData rsmd = resultSet.getMetaData();

            int line = 1;
            System.out.printf("TABLE %s\n", "MOVIE");
            System.out.printf("%c, %s\t, \t%s\n", '#', rsmd.getColumnName(1), rsmd.getColumnName(2));
            while (resultSet.next()) {
                System.out.printf("%d, %d\t, %s\n",
                        line++, resultSet.getLong(1), resultSet.getString(2));
            }
        } catch (SQLException e) {
            throw new DAOException("Failed connection " + e.getMessage());
        } finally {
            DAOConnectionPool.getInstance().releaseConnection(connection);
        }
    }
}
