package dao;


import dao.connection.DAOConnectionPool;
import entity.*;
import entity.enumeration.PaymentStatus;
import entity.enumeration.TicketStatus;
import exception.DAOException;

import java.sql.*;
import java.time.Instant;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;
import java.util.logging.Logger;

public class TicketDAO implements BaseDAO<Ticket, Long> {

    private final Logger log = Logger.getLogger(this.getClass().getName());

    private static final String SQL_GET_ALL = "SELECT * FROM ticket";
    private static final String SQL_GET_BY_USER_LOGIN = "select * from ticket t " +
            "join app_user u on t.user_login=u.phone and u.phone = ?  join movie_session ms on " +
            "t.movie_session_id = ms.id where  t.status = 'NEW'";
    private static final String SQL_GET_TICKETS_PAYMENT_PROCESSING_STATUS = "select movie_session_id, trow, seat," +
            " user_login, status,  created_at, payment_code from ticket where status = 'PAYMENT_PROCESSING'";
    private static final String SQL_GET_PAYMENT_STATUS_BY_CODE = "select p.status from ticket t " +
            "join payment p on t.payment_code=p.payment_code and t.payment_code=?";
    private static final String SQL_SAVE = "INSERT INTO ticket (trow, seat, status, created_at," +
            " movie_session_id, user_login ) VALUES (?,?,?,?,?,?)";
    private static final String SQL_UPDATE = "UPDATE ticket SET  status = ?, payment_code = ?::uuid," +
            " status_updated_at = now() WHERE user_login = ? and movie_session_id = ? and trow = ? and seat = ?";


    @Override
    public List<Ticket> findAll() throws DAOException{
        log.info("Searching all tickets");
        log.info("Opening a database connection");
        Connection connection = DAOConnectionPool.getInstance().getConnection();
        List<Ticket> tickets = new ArrayList<>();
        try (PreparedStatement statement = connection.prepareStatement(SQL_GET_ALL);
             ResultSet resultSet = statement.executeQuery()) {
            log.info("Connection established");
            while (resultSet.next()) {
                Ticket ticket = new Ticket();
                ticket.setRow(resultSet.getByte("trow"));
                ticket.setSeat(resultSet.getByte("seat"));
                ticket.setStatus(TicketStatus.valueOf(resultSet.getString("status")));
                ticket.setCreatedAt(resultSet.getTimestamp("created_at").toLocalDateTime());
                ticket.setStatusUpdatedAt(resultSet.getTimestamp("status_updated_at").toLocalDateTime());
                ticket.setPaymentCode((UUID) resultSet.getObject("payment_code"));

                User user = new User();
                user.setPhone(resultSet.getString("user_login"));

                MovieSession movieSession = new MovieSession();
                movieSession.setId(resultSet.getLong("movie_session_id"));

                ticket.setUser(user);
                ticket.setMovieSession(movieSession);

                tickets.add(ticket);
            }
        } catch (SQLException e) {
            throw new DAOException("Failed connection " + e.getMessage());
        } finally {
            if (connection != null) {
                DAOConnectionPool.getInstance().releaseConnection(connection);
            }
        }
        log.info("All tickets found");
        return tickets;
    }

    @Override
    public void save(Ticket ticket) throws DAOException{
        log.info("Saving ticket");
        Connection connection = DAOConnectionPool.getInstance().getConnection();
        try (PreparedStatement statement = connection.prepareCall(SQL_SAVE)) {

            log.info("Create prepared statement");

            statement.setByte(1, ticket.getRow());
            statement.setByte(2, ticket.getSeat());
            statement.setString(3, ticket.getStatus().toString());
            statement.setTimestamp(4, Timestamp.from(Instant.now()));
            statement.setLong(5, ticket.getMovieSession().getId());
            statement.setString(6, ticket.getUser().getPhone());

            statement.executeUpdate();
            log.info("Statement created");

        } catch (SQLException e) {
            throw new DAOException(e.getMessage(), e);
        } finally {
            if (connection != null) {
                DAOConnectionPool.getInstance().releaseConnection(connection);
            }
        }

        log.info("Ticket saved");
    }


    public void update(Ticket ticket) throws DAOException{
        log.info("Updating ticket");
        log.info("Opening a database connection");
        Connection connection = DAOConnectionPool.getInstance().getConnection();

        try (PreparedStatement statement = connection.prepareStatement(SQL_UPDATE)) {

            log.info("Create statement");

            statement.setString(1, ticket.getStatus().name());
            statement.setString(2, ticket.getPaymentCode().toString());
            statement.setString(3, ticket.getUser().getPhone());
            statement.setLong(4, ticket.getMovieSession().getId());
            statement.setByte(5, ticket.getRow());
            statement.setByte(6, ticket.getSeat());

            statement.executeUpdate();
            log.info("Statement created");

        } catch (SQLException e) {
            log.severe("Failed to close connection with database" + e.getMessage());
            throw new DAOException("Cannot connect to the database", e);
        } finally {
            if (connection != null) {
                DAOConnectionPool.getInstance().releaseConnection(connection);
            }
        }
        log.info("Ticket updated successfully");
    }

    public Ticket getTicketByUserLogin(String userLogin) throws DAOException{
        log.info("Searching ticket where user login " + userLogin);
        Ticket ticket = new Ticket();
        PreparedStatement statement = null;
        ResultSet resultSet = null;
        Connection connection = DAOConnectionPool.getInstance().getConnection();

        log.info("Opening a database connection");
        try {
            statement = connection.prepareStatement(SQL_GET_BY_USER_LOGIN);
            statement.setString(1, userLogin);
            log.info("Statement created");

            resultSet = statement.executeQuery();
            while (resultSet.next()) {
                ticket = new Ticket();
                ticket.setRow(resultSet.getByte("trow"));
                ticket.setSeat(resultSet.getByte("seat"));
                ticket.setStatus(TicketStatus.valueOf(resultSet.getString("status")));
                ticket.setCreatedAt(resultSet.getTimestamp("created_at").toLocalDateTime());
                ticket.setPaymentCode((UUID) resultSet.getObject("payment_code"));

                User user = new User();
                user.setFullName(resultSet.getString("full_name"));
                user.setEmail(resultSet.getString("email"));
                user.setPhone(resultSet.getString("phone"));

                MovieSession movieSession = new MovieSession();
                movieSession.setId(resultSet.getLong("movie_session_id"));

                ticket.setMovieSession(movieSession);
                ticket.setUser(user);
            }
        } catch (SQLException e) {
            throw new DAOException("Cannot connect to the database", e);
        } finally {
            try {
                if (resultSet != null) {
                    resultSet.close();
                    log.info("Result set closed");
                }
                if (statement != null) {
                    statement.close();
                    log.info("Statement closed");
                }
                if (connection != null) {
                    DAOConnectionPool.getInstance().releaseConnection(connection);
                }
                log.info("Connection to DB closed");
            } catch (SQLException e) {
                log.severe("Failed to close connection with database" + e.getMessage());
                throw new DAOException("Cannot close connection");
            }
        }
        log.info("Ticket with user login " + userLogin + " found");
        return ticket;
    }

    public List<Ticket> getAllTicketsPaymentProcessingStatus() throws DAOException{
        log.info("Searching tickets where status = " + TicketStatus.PAYMENT_PROCESSING);
        List<Ticket> tickets = new ArrayList<>();
        PreparedStatement statement = null;
        ResultSet resultSet = null;
        Connection connection = DAOConnectionPool.getInstance().getConnection();
        log.info("Opening a database connection");
        try {
            statement = connection.prepareStatement(SQL_GET_TICKETS_PAYMENT_PROCESSING_STATUS);
            resultSet = statement.executeQuery();

            while (resultSet.next()) {
                Ticket ticket = new Ticket();
                ticket.setRow(resultSet.getByte("trow"));
                ticket.setSeat(resultSet.getByte("seat"));
                ticket.setCreatedAt(resultSet.getTimestamp("created_at").toLocalDateTime());
                ticket.setStatus(TicketStatus.valueOf(resultSet.getString("status")));
                ticket.setPaymentCode(UUID.fromString(resultSet.getString("payment_code")));

                User user = new User();
                user.setPhone(resultSet.getString("user_login"));

                MovieSession movieSession = new MovieSession();
                movieSession.setId(resultSet.getLong("movie_session_id"));

                ticket.setUser(user);
                ticket.setMovieSession(movieSession);

                tickets.add(ticket);
            }
        } catch (SQLException e) {
            throw new DAOException("Failed connection " + e.getMessage());
        } finally {
            try {
                if (resultSet != null) {
                    resultSet.close();
                }
                if (statement != null) {
                    statement.close();
                }
                if (connection != null) {
                    DAOConnectionPool.getInstance().releaseConnection(connection);
                }
                log.info("Connection to DB closed");
            } catch (SQLException e) {
                log.severe("Failed to close connection with database" + e.getMessage());
                throw new DAOException("Cannot close connection");
            }
        }
        log.info("Tickets with status = PAYMENT_PROCESSING found");
        return tickets;
    }

    public PaymentStatus getPaymentStatusByCode(UUID paymentCode) throws DAOException{
        log.info("Searching payment status where payment code in ticket  = " + paymentCode);
        PaymentStatus status = null;
        ResultSet resultSet = null;
        Connection connection = DAOConnectionPool.getInstance().getConnection();

        log.info("Opening a database connection");
        try (PreparedStatement statement = connection.prepareStatement(SQL_GET_PAYMENT_STATUS_BY_CODE)) {

            statement.setObject(1, paymentCode);
            resultSet = statement.executeQuery();
            while (resultSet.next()) {
                status = PaymentStatus.valueOf(resultSet.getString("status"));
            }
        } catch (SQLException e) {
            throw new DAOException("Failed connection " + e.getMessage());
        } finally {
            if (resultSet != null) {
                try {
                    resultSet.close();
                } catch (SQLException e) {
                    throw new DAOException("Failed to close ResultSet " + e.getMessage());
                }
                log.info("Result set closed");
            }
            if (connection != null) {
                DAOConnectionPool.getInstance().releaseConnection(connection);
            }
        }
        log.info("Payment status found");
        return status;
    }

    @Override
    public void printAll() throws DAOException {
        log.info("Searching all cinema to print");
        log.info("Opening a database connection");
        Connection connection = DAOConnectionPool.getInstance().getConnection();

        try (PreparedStatement statement = connection.prepareStatement(SQL_GET_ALL);
             ResultSet resultSet = statement.executeQuery()) {
            log.info("Connection established");
            ResultSetMetaData rsmd = resultSet.getMetaData();

            int line = 1;
            System.out.printf("TABLE %s\n", "TICKET");
            while (resultSet.next()) {
                System.out.printf("%d, %d, %d, %d, %s, %s, %1tD %1tT, %<tD %<tT, %s\n",
                        line++, resultSet.getLong(1), resultSet.getByte(2),
                        resultSet.getByte(3), resultSet.getString(4), resultSet.getString(5),
                        resultSet.getTimestamp(6), resultSet.getTimestamp(7), resultSet.getString(8));
            }
        } catch (SQLException e) {
            throw new DAOException("Failed connection " + e.getMessage());
        } finally {
            DAOConnectionPool.getInstance().releaseConnection(connection);
        }
    }
}
