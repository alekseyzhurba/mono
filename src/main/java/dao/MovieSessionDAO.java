package dao;


import dao.connection.DAOConnectionPool;
import entity.Cinema;
import entity.Movie;
import entity.MovieSession;
import exception.DAOException;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Logger;

public class MovieSessionDAO implements BaseDAO<MovieSession, Long> {

    private final Logger log = Logger.getLogger(this.getClass().getName());

    private static final String SQL_SAVE = "INSERT INTO movie_session VALUES (?,?,?,?,?)";
    private static final String SQL_GET_ALL = "SELECT * FROM movie_session";


    @Override
    public List<MovieSession> findAll() throws DAOException{
        log.info("Searching all movie sessions");
        log.info("Opening a database connection");
        Connection connection = DAOConnectionPool.getInstance().getConnection();
        List<MovieSession> movieSessions = new ArrayList<>();
        try (PreparedStatement statement = connection.prepareStatement(SQL_GET_ALL);
             ResultSet resultSet = statement.executeQuery()) {
            log.info("Connection established");
            while (resultSet.next()) {
                MovieSession movieSession = new MovieSession();
                movieSession.setId(resultSet.getLong("id"));
                movieSession.setSessionTime((resultSet.getTimestamp("session_time")).toLocalDateTime());
                movieSession.setPrice(resultSet.getDouble("price"));

                Movie movie = new Movie();
                movie.setId(resultSet.getLong("movie_id"));

                Cinema cinema = new Cinema();
                cinema.setId(resultSet.getLong("cinema_id"));

                movieSession.setMovie(movie);
                movieSession.setCinema(cinema);

                movieSessions.add(movieSession);
            }
        } catch (SQLException e) {
            throw new DAOException("Failed connection " + e.getMessage());
        } finally {
            if (connection != null) {
                DAOConnectionPool.getInstance().releaseConnection(connection);
            }
        }
        log.info("Movie sessions found");
        return movieSessions;
    }

    @Override
    public void save(MovieSession entity) throws DAOException{
        log.info("Saving movie session");
        log.info("Opening a database connection to save movie session");
        Connection connection = DAOConnectionPool.getInstance().getConnection();
        try (PreparedStatement statement = connection.prepareStatement(SQL_SAVE)) {
            log.info("Connection established");

            statement.setLong(1, entity.getId());
            statement.setLong(2, entity.getMovie().getId());
            statement.setLong(3, entity.getCinema().getId());
            statement.setTimestamp(4, Timestamp.valueOf(entity.getSessionTime()));
            statement.setDouble(5, entity.getPrice());

            statement.executeUpdate();
            log.info("Statement created");

        } catch (SQLException e) {
            throw new DAOException("Cannot connect to the database", e);
        } finally {
            if (connection != null) {
                DAOConnectionPool.getInstance().releaseConnection(connection);
            }
        }
        log.info("Movie session saved successfully");
    }

    @Override
    public void printAll() throws DAOException {
        log.info("Searching all cinema to print");
        log.info("Opening a database connection");
        Connection connection = DAOConnectionPool.getInstance().getConnection();
        try (PreparedStatement statement = connection.prepareStatement(SQL_GET_ALL);
             ResultSet resultSet = statement.executeQuery()) {
            log.info("Connection established");
            ResultSetMetaData rsmd = resultSet.getMetaData();

            int line = 1;
            System.out.printf("TABLE %s\n", "MOVIE SESSION");
            System.out.printf("%c, %s\t, %s\t, %s\t\t\t, %s , %s\n", '#',
                    rsmd.getColumnName(1),
                    rsmd.getColumnName(2),
                    rsmd.getColumnName(3),
                    rsmd.getColumnName(4),
                    rsmd.getColumnName(5));
            while (resultSet.next()) {
                System.out.printf("%d, %d\t, %d\t, %d\t\t\t, %<tD %tT, %.2f\n",
                        line++, resultSet.getLong(1), resultSet.getLong(2),
                        resultSet.getLong(3), resultSet.getTimestamp(4), resultSet.getDouble(5));
            }
        } catch (SQLException e) {
            throw new DAOException("Failed connection " + e.getMessage());
        } finally {
            DAOConnectionPool.getInstance().releaseConnection(connection);
        }
    }
}