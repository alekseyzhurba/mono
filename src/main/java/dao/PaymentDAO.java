package dao;


import dao.connection.DAOConnectionPool;
import entity.*;
import entity.enumeration.PaymentStatus;
import entity.enumeration.TicketStatus;
import exception.DAOException;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;
import java.util.logging.Logger;

public class PaymentDAO implements BaseDAO<Payment, Long> {

    private final Logger log = Logger.getLogger(this.getClass().getName());

    private static final String SQL_PAYMENT_STATUS_NEW = "SELECT * FROM payment WHERE status = 'NEW'";
    private static final String SQL_SAVE = "INSERT INTO payment VALUES (?,?,?,?)";
    private static final String SQL_UPDATE = "UPDATE payment SET status = ? WHERE payment_code = ?";
    private static final String SQL_GET_ALL = "SELECT * FROM payment";

    private TicketDAO ticketDAO;
    private UserDAO userDAO;

    public PaymentDAO() {
        ticketDAO = new TicketDAO();
        userDAO = new UserDAO();
    }


    @Override
    public List<Payment> findAll() throws DAOException {
        log.info("Searching all payments");
        log.info("Opening a database connection");
        Connection connection = DAOConnectionPool.getInstance().getConnection();
        List<Payment> payments = new ArrayList<>();
        try (PreparedStatement statement = connection.prepareStatement(SQL_GET_ALL);
             ResultSet resultSet = statement.executeQuery()) {
            log.info("Connection established");
            while (resultSet.next()) {
                Payment payment = new Payment();
                payment.setPaymentCode(UUID.fromString(resultSet.getString("payment_code")));
                payment.setAmount(resultSet.getDouble("amount"));
                payment.setCardNum(resultSet.getString("card_num"));
                payment.setStatus(PaymentStatus.valueOf(resultSet.getString("status")));

                payments.add(payment);
            }
        } catch (SQLException e) {
            throw new DAOException("Failed connection " + e.getMessage());
        } finally {
            DAOConnectionPool.getInstance().releaseConnection(connection);
        }
        log.info("Payments found");
        return payments;
    }

    @Override
    public void printAll() throws DAOException {
        log.info("Searching all payments to print");
        log.info("Opening a database connection");
        Connection connection = DAOConnectionPool.getInstance().getConnection();
        try (PreparedStatement statement = connection.prepareStatement(SQL_GET_ALL);
             ResultSet resultSet = statement.executeQuery()) {
            log.info("Connection established");
            ResultSetMetaData rsmd = resultSet.getMetaData();

            int line = 1;
            System.out.printf("TABLE %s\n", "PAYMENT");
            System.out.printf("%c,\t%s\t\t\t\t\t\t, %s, %s, %s\n", '#',
                    rsmd.getColumnName(1),
                    rsmd.getColumnName(2),
                    rsmd.getColumnName(3),
                    rsmd.getColumnName(4));
            while (resultSet.next()) {
                System.out.printf("%d, %s, %.2f, %s, %s\n",
                        line++, resultSet.getString(1), resultSet.getDouble(2),
                        resultSet.getString(3), resultSet.getString(4));
            }
        } catch (SQLException e) {
            throw new DAOException("Failed connection " + e.getMessage());
        } finally {
            DAOConnectionPool.getInstance().releaseConnection(connection);
        }
    }

    @Override
    public void save(Payment payment) throws DAOException {
        log.info("Saving payment");
        log.info("Opening a database connection");
        Connection connection = DAOConnectionPool.getInstance().getConnection();
        try (PreparedStatement statement = connection.prepareStatement(SQL_SAVE)) {


            statement.setObject(1, payment.getPaymentCode());
            statement.setDouble(2, payment.getAmount());
            statement.setString(3, payment.getCardNum());
            statement.setString(4, PaymentStatus.NEW.name());
            statement.executeUpdate();
            log.info("Statement created");

        } catch (SQLException e) {
            throw new DAOException("Failed connection " + e.getMessage());
        } finally {
            if (connection != null) {
                DAOConnectionPool.getInstance().releaseConnection(connection);
            }
        }
        log.info("Payment saved");

        String login = userDAO.getLoginByCard(payment.getCardNum());

        Ticket ticket = ticketDAO.getTicketByUserLogin(login);

        ticket.setPaymentCode(payment.getPaymentCode());
        ticket.setStatus(TicketStatus.PAYMENT_PROCESSING);
        ticketDAO.update(ticket);
    }


    public void update(Payment payment) throws DAOException {
        log.info("Updating payment where code " + payment.getPaymentCode());
        log.info("Opening a database connection");
        Connection connection = DAOConnectionPool.getInstance().getConnection();
        try (PreparedStatement statement = connection.prepareStatement(SQL_UPDATE)) {

            log.info("Create statement");
            statement.setString(1, payment.getStatus().name());
            statement.setObject(2, (payment.getPaymentCode()));
            statement.executeUpdate();
            log.info("Statement created");

        } catch (SQLException e) {
            log.severe("Failed to close connection with database" + e.getMessage());
            throw new DAOException("Failed connection", e);
        } finally {
            if (connection != null) {
                DAOConnectionPool.getInstance().releaseConnection(connection);
            }
        }
    }


    public List<Payment> getAllPaymentWithStatusNew() throws DAOException {
        log.info("Searching payments where status = " + PaymentStatus.NEW);
        List<Payment> payments = new ArrayList<>();
        Connection connection = DAOConnectionPool.getInstance().getConnection();
        log.info("Opening a database connection");
        try (PreparedStatement statement = connection.prepareStatement(SQL_PAYMENT_STATUS_NEW);
             ResultSet resultSet = statement.executeQuery()) {

            while (resultSet.next()) {
                Payment payment = new Payment();
                payment.setPaymentCode(UUID.fromString(resultSet.getString("payment_code")));
                payment.setAmount(resultSet.getDouble("amount"));
                payment.setCardNum(resultSet.getString("card_num"));
                payment.setStatus(PaymentStatus.valueOf(resultSet.getString("status")));

                payments.add(payment);
            }
        } catch (SQLException e) {
            throw new DAOException("Failed connection " + e.getMessage());
        } finally {
            if (connection != null) {
                DAOConnectionPool.getInstance().releaseConnection(connection);
            }
        }
        log.info("All payments with status NEW found");
        return payments;
    }
}
