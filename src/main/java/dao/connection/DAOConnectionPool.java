package dao.connection;

import exception.DAOException;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.LinkedBlockingDeque;

public class DAOConnectionPool {

    private static DAOConnectionPool instance;

    private BlockingQueue<Connection> freeConnection;

    private DAOProperties daoProperties;

    private static final String DATABASE_URL = "url";
    private static final String DATABASE_DRIVER = "db.driver";
    private static final String DATABASE_USERNAME = "username";
    private static final String DATABASE_PASSWORD = "password";

    private final static int POOL_SIZE = 10;

    public static DAOConnectionPool getInstance() {
        if (instance == null)
            synchronized (DAOConnectionPool.class) {
                instance = new DAOConnectionPool();
            }
        return instance;
    }

    private DAOConnectionPool() {
        freeConnection = new LinkedBlockingDeque<>(POOL_SIZE);
        daoProperties = new DAOProperties();
        initPool();
    }

    public void releaseConnection(Connection connection) {
        freeConnection.offer(connection);
    }

    public Connection getConnection() {
        Connection connection = null;
        try {
            connection = freeConnection.take();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        return connection;
    }

    public void destroyConnections() {
        for (int i = 0; i < POOL_SIZE; i++) {
            try {
                freeConnection.take().close();
            } catch (SQLException | InterruptedException e) {
                throw new DAOException("Unexpected behavior on close connection pool " + e.getMessage());
            }
        }
    }

    private void initPool() {
        for (int i = 0; i < POOL_SIZE; i++) {
            Connection connection = null;

            String url = daoProperties.getProperties().getProperty(DATABASE_URL);
            String user = daoProperties.getProperties().getProperty(DATABASE_USERNAME);
            String password = daoProperties.getProperties().getProperty(DATABASE_PASSWORD);

            try {
                connection = DriverManager.getConnection(url, user, password);
            } catch (SQLException e) {
                throw new DAOException("Wrong connection properties " + e.getMessage());
            }
            freeConnection.offer(connection);
        }
    }
}
