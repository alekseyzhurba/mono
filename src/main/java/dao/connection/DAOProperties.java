package dao.connection;


import exception.DAOConfigurationException;

import java.io.*;
import java.util.Properties;

public class DAOProperties {

    private static final String PROPERTIES_FILE = "/db.properties";
    private Properties properties;
    private InputStreamReader fileReader;

    public Properties getProperties() {
        if (properties == null) {
            properties = new Properties();
            try {
                properties.load(getPropertiesFile());
            } catch (IOException e) {
                throw new DAOConfigurationException("Can't load properties file '" + PROPERTIES_FILE + "'.", e);
            }
        }
        return properties;
    }

    private InputStreamReader getPropertiesFile() {
        if (fileReader == null) {
            try {
                fileReader = new InputStreamReader(getClass().getResourceAsStream(PROPERTIES_FILE));
            } catch (IllegalArgumentException e) {
                throw new DAOConfigurationException(
                        "Path to properties file '" + PROPERTIES_FILE + "' is missing or incorrect.", e);
            }
        }
        return fileReader;
    }
}
