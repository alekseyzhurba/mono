package dao;

import exception.DAOException;

import java.util.List;

public interface BaseDAO<T, ID> {

    List<T> findAll() throws DAOException;

    void save(T entity) throws DAOException;

    void printAll();
}