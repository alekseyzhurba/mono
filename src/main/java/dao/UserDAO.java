package dao;


import dao.connection.DAOConnectionPool;
import entity.User;
import exception.DAOException;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Logger;

public class UserDAO implements BaseDAO<User, String> {

    private final Logger log = Logger.getLogger(this.getClass().getName());

    private static final String SQL_GET_ALL = "SELECT * FROM app_user";
    private static final String SQL_GET_BY_CARD = "SELECT phone FROM app_user WHERE full_name = ?";

    private static final String SQL_SAVE = "INSERT INTO app_user VALUES (?,?,?)";


    public String getLoginByCard(String card) throws DAOException {
        log.info("Looking for user with card num " + card);
        String userCard = null;
        Connection connection = DAOConnectionPool.getInstance().getConnection();
        ResultSet resultSet = null;
        log.info("Opening a database connection");
        try (PreparedStatement statement = connection.prepareStatement(SQL_GET_BY_CARD);) {
            log.info("Connection established");

            statement.setString(1, card);
            log.info("Statement created");
            resultSet = statement.executeQuery();

            while (resultSet.next()) {
                userCard = resultSet.getString("phone");
            }
            log.info("Result set received");
        } catch (SQLException e) {
            throw new DAOException("Cannot connect to the database", e);
        } finally {
            try {
                if (resultSet != null) {
                    resultSet.close();
                    log.info("Result set closed");
                }
                if (connection != null) {
                    DAOConnectionPool.getInstance().releaseConnection(connection);
                }
                log.info("Connection to DB closed");
            } catch (SQLException e) {
                log.severe("Failed to close connection with database" + e.getMessage());
                throw new DAOException("Cannot close connection");
            }
        }
        return userCard;
    }

    @Override
    public List<User> findAll() throws DAOException {
        log.info("Searching all users");
        log.info("Opening a database connection");
        Connection connection = DAOConnectionPool.getInstance().getConnection();
        List<User> users = new ArrayList<>();
        try (PreparedStatement statement = connection.prepareStatement(SQL_GET_ALL);
             ResultSet resultSet = statement.executeQuery()) {
            log.info("Connection established");
            while (resultSet.next()) {
                User user = new User();
                user.setFullName(resultSet.getString("full_name"));
                user.setEmail(resultSet.getString("email"));
                user.setPhone(resultSet.getString("phone"));

                users.add(user);
            }
        } catch (SQLException e) {
            throw new DAOException("Failed connection " + e.getMessage());
        } finally {
            if (connection != null) {
                DAOConnectionPool.getInstance().releaseConnection(connection);
            }
        }
        log.info("Users found");
        return users;
    }

    @Override
    public void save(User entity) throws DAOException {
        log.info("Saving user");
        log.info("Opening a database connection");
        Connection connection = DAOConnectionPool.getInstance().getConnection();

        try (PreparedStatement statement = connection.prepareStatement(SQL_SAVE);) {
            log.info("Connection established");

            statement.setString(1, entity.getFullName());
            statement.setString(2, entity.getEmail());
            statement.setString(3, entity.getPhone());

            statement.executeUpdate();
            log.info("Statement created");

        } catch (SQLException e) {
            throw new DAOException("Cannot connect to the database", e);
        } finally {
            if (connection != null) {
                DAOConnectionPool.getInstance().releaseConnection(connection);
            }
        }
        log.info("User saved successfully");
    }

    @Override
    public void printAll() throws DAOException {
        log.info("Searching all cinema to print");
        log.info("Opening a database connection");
        Connection connection = DAOConnectionPool.getInstance().getConnection();

        try (PreparedStatement statement = connection.prepareStatement(SQL_GET_ALL);
             ResultSet resultSet = statement.executeQuery()) {
            log.info("Connection established");

            int line = 1;
            System.out.printf("TABLE %s\n", "USER");
            while (resultSet.next()) {
                System.out.printf("%d, %s, %s, %s\n",
                        line++, resultSet.getString(1), resultSet.getString(2), resultSet.getString(3));
            }
        } catch (SQLException e) {
            throw new DAOException("Failed connection " + e.getMessage());
        } finally {
            DAOConnectionPool.getInstance().releaseConnection(connection);
        }
    }
}
