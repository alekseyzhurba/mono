
import service.FileService;

import java.io.*;

import java.util.logging.*;


public class CinemaJDBC {
    private static Logger log = Logger.getLogger(CinemaJDBC.class.getName());
    private static final String LOG_FILE = "entity.txt";
    private static final String ENTITY_FILE = "log.txt";
    private static final String PATH_TO_STARTFILE = "/home/madmin/Документы/java/курсы mono/file/file.txt";

    public static void main(String[] args) throws InterruptedException, IOException {
        LogManager.getLogManager().readConfiguration(CinemaJDBC.class.getResourceAsStream("logging.properties"));

        FileOutputStream f = new FileOutputStream("entity.txt", false);
        System.setOut(new PrintStream(f));

        FileService fileService = new FileService();
        fileService.readFile(PATH_TO_STARTFILE);

        fileService.crutch(LOG_FILE, ENTITY_FILE);

    }
}

