package entity;


import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;

import java.time.LocalDateTime;

@Data
@ToString
@EqualsAndHashCode
public class MovieSession {

    private Long id;

    private Movie movie;

    private Cinema cinema;

    private LocalDateTime sessionTime;

    private Double price;
}
