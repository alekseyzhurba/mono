package entity;


import entity.enumeration.TicketStatus;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.time.Instant;
import java.time.LocalDateTime;
import java.util.UUID;


@Data
@EqualsAndHashCode
public class Ticket {

    private MovieSession movieSession;

    private Byte row;

    private Byte seat;

    private User user;

    private TicketStatus status;

    private LocalDateTime createdAt;

    private LocalDateTime statusUpdatedAt;

    private UUID paymentCode;
}
