package entity;


import entity.enumeration.PaymentStatus;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.util.UUID;

@Data
@EqualsAndHashCode
public class Payment {

    private UUID paymentCode;

    private Double amount;

    private String cardNum;

    private PaymentStatus status;
}
