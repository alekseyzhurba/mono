package entity;

import lombok.*;


@Data
@EqualsAndHashCode
@ToString
public class User {

    private String fullName;

    private String email;

    private String phone;
}
