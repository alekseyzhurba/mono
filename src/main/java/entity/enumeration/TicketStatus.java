package entity.enumeration;

/*
 * @autor madmin
 */

import lombok.EqualsAndHashCode;


public enum TicketStatus {
    NEW, PAYMENT_PROCESSING, PAID, REJECTED
}
