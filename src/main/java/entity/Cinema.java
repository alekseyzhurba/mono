package entity;


import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;


@Data
@ToString
@EqualsAndHashCode
public class Cinema {

    private Long id;

    private String name;

    private Byte rowCount;

    private Byte seatsInRow;
}
