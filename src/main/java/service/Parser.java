package service;


import dao.*;
import dao.connection.DAOConnectionPool;
import entity.*;
import entity.enumeration.PaymentStatus;
import entity.enumeration.TicketStatus;


import java.io.BufferedOutputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.PrintStream;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;
import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;

import static java.lang.Thread.MAX_PRIORITY;

public class Parser {


    public void parseLine(String line) throws InterruptedException {
        String[] parsedLine = line.split("\\|");

        for (int i = 0; i < parsedLine.length; i++) {
            switch (parsedLine[i]) {
                case "REGISTRATION":
                    createUser(parsedLine);
                    break;
                case "TICKET":
                    createTicket(parsedLine);
                    break;
                case "MOVIE":
                    createMovie(parsedLine);
                    break;
                case "CINEMA":
                    createCinema(parsedLine);
                    break;
                case "MOVIE_SESSION":
                    createMovieSession(parsedLine);
                    break;
                case "PAYMENT":
                    createPayment(parsedLine);
                    break;
                case "START":
                    ArrayBlockingQueue<UUID> queue = new ArrayBlockingQueue<>(5);
                    TicketService ticketService = new TicketService(queue);
                    PaymentService paymentService = new PaymentService(queue);

                    ExecutorService service = Executors.newFixedThreadPool(2);
                    service.execute(paymentService);
                    service.execute(ticketService);

                    service.shutdown();
                    service.awaitTermination(MAX_PRIORITY, TimeUnit.HOURS);
                case "SHOW_RESULTS":
                    new MovieDAO().printAll();
                    new MovieSessionDAO().printAll();
                    new CinemaDAO().printAll();
                    new UserDAO().printAll();
                    new TicketDAO().printAll();
                    new PaymentDAO().printAll();




                    DAOConnectionPool.getInstance().destroyConnections();
            }
        }
    }

    private void createUser(String[] parsedLine) {
        User user = new User();
        for (int i = 0; i < parsedLine.length; i++) {
            user.setFullName(parsedLine[++i]);
            user.setEmail(parsedLine[++i]);
            user.setPhone(parsedLine[++i]);
        }
        UserDAO userDAO = new UserDAO();
        userDAO.save(user);
    }

    private void createPayment(String[] parsedLine) {
        Payment payment = new Payment();
        for (int i = 0; i < parsedLine.length; i++) {
            payment.setPaymentCode(UUID.fromString(parsedLine[++i]));
            payment.setAmount(Double.parseDouble(parsedLine[++i]));
            payment.setCardNum((parsedLine[++i]));
            payment.setStatus(PaymentStatus.valueOf(parsedLine[++i]));
        }
        PaymentDAO paymentDAO = new PaymentDAO();
        paymentDAO.save(payment);
    }

    private void createMovieSession(String[] parsedLine) {
        MovieSession movieSession = new MovieSession();
        for (int i = 0; i < parsedLine.length; i++) {
            movieSession.setId(Long.valueOf(parsedLine[++i]));
            Movie movie = new Movie();
            movie.setId(Long.valueOf(parsedLine[++i]));
            movieSession.setMovie(movie);
            Cinema cinema = new Cinema();
            cinema.setId(Long.valueOf(parsedLine[++i]));
            movieSession.setCinema(cinema);
            movieSession.setSessionTime(LocalDateTime.parse(parsedLine[++i]));
            movieSession.setPrice(Double.parseDouble(parsedLine[++i]));
        }
        MovieSessionDAO movieSessionDAO = new MovieSessionDAO();
        movieSessionDAO.save(movieSession);
    }

    private void createCinema(String[] parsedLine) {
        Cinema cinema = new Cinema();
        for (int i = 0; i < parsedLine.length; i++) {
            cinema.setId(Long.valueOf(parsedLine[++i]));
            cinema.setName(parsedLine[++i]);
            cinema.setRowCount(Byte.valueOf(parsedLine[++i]));
            cinema.setSeatsInRow(Byte.valueOf(parsedLine[++i]));
        }
        CinemaDAO cinemaDAO = new CinemaDAO();
        cinemaDAO.save(cinema);
    }

    private void createTicket(String[] parsedLine) {
        Ticket ticket = new Ticket();
        for (int i = 0; i < parsedLine.length; i++) {
            MovieSession movieSession = new MovieSession();
            movieSession.setId(Long.valueOf(parsedLine[++i]));
            ticket.setMovieSession(movieSession);
            ticket.setRow(Byte.valueOf(parsedLine[++i]));
            ticket.setSeat(Byte.valueOf(parsedLine[++i]));
            User user1 = new User();
            user1.setPhone(parsedLine[++i]);
            ticket.setUser(user1);
            ticket.setStatus(TicketStatus.valueOf(parsedLine[++i]));
            ticket.setCreatedAt(LocalDateTime.now());
        }
        TicketDAO ticketDAO = new TicketDAO();
        ticketDAO.save(ticket);
    }

    private void createMovie(String[] parsedLine) {
        Movie movie = new Movie();
        for (int i = 0; i < parsedLine.length; i++) {
            movie.setId(Long.valueOf(parsedLine[++i]));
            movie.setTitle(parsedLine[++i]);
        }
        MovieDAO movieDAO = new MovieDAO();
        movieDAO.save(movie);
    }
}
