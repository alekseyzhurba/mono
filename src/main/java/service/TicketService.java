package service;

import dao.TicketDAO;
import entity.Ticket;
import entity.enumeration.PaymentStatus;
import entity.enumeration.TicketStatus;

import java.util.Collections;
import java.util.List;
import java.util.UUID;
import java.util.concurrent.ArrayBlockingQueue;
import java.util.logging.Logger;


public class TicketService implements Runnable {

    private ArrayBlockingQueue<UUID> queue;
    private final TicketDAO ticketDAO;

    private final Logger log = Logger.getLogger(this.getClass().getName());

    public TicketService(ArrayBlockingQueue<UUID> queue) {
        this.queue = queue;
        ticketDAO = new TicketDAO();
    }

    @Override
    public void run() {
        // TODO: 16.02.2022 изменить на один метод
        List<Ticket> ticketsForPayment = Collections.synchronizedList(ticketDAO.getAllTicketsPaymentProcessingStatus());
        UUID processingCode = null;

        log.info("Processing ticket");
        while (!ticketsForPayment.isEmpty()) {
            Ticket tmp = null;
            try {
                processingCode = queue.take();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }

            for (Ticket ticket : ticketsForPayment) {
                tmp = ticket;
                if (ticket.getPaymentCode().equals(processingCode)) {
                    log.info("Ticket with payment code " + ticket.getPaymentCode() +  " processed");
                    PaymentStatus status = ticketDAO.getPaymentStatusByCode(ticket.getPaymentCode());
                    ticketProcessing(ticket, status);
                    break;
                }
            }
            ticketsForPayment.remove(tmp);

            if (ticketsForPayment.isEmpty()) {
                log.info("Check for appeared tickets");
                haveNewTickets(ticketsForPayment);
            }
        }
    }

    private synchronized void ticketProcessing(Ticket ticket, PaymentStatus status) {
        switch (status) {
            case PROVIDED:
                ticket.setStatus(TicketStatus.PAID);
                ticketDAO.update(ticket);
                break;
            case NOT_PASSED:
                ticket.setStatus(TicketStatus.REJECTED);
                ticketDAO.update(ticket);
                break;
            default:
                break;
        }
    }

    private synchronized void haveNewTickets(List<Ticket> ticketsForPayment){

        List<Ticket> appearedTickets = ticketDAO.getAllTicketsPaymentProcessingStatus();
        if(appearedTickets.isEmpty()){
            log.info("New tickets not found");
            return;
        }else {
            ticketsForPayment.addAll(appearedTickets);
        }
    }
}
