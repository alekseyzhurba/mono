package service;


import dao.PaymentDAO;
import entity.Payment;
import entity.enumeration.PaymentStatus;


import java.util.*;
import java.util.concurrent.ArrayBlockingQueue;
import java.util.logging.Logger;

public class PaymentService implements Runnable {

    private PaymentDAO paymentDAO;
    private ArrayBlockingQueue queue;

    private final Logger log = Logger.getLogger(this.getClass().getName());

    public PaymentService(ArrayBlockingQueue queue) {
        this.queue = queue;
        paymentDAO = new PaymentDAO();
    }

    @Override
    public void run() {
        paymentProcess();
    }

    private synchronized void paymentProcess() {
        List<Payment> paymentForProcessing = paymentDAO.getAllPaymentWithStatusNew();
        List<UUID> processedPayments = paymentGateway(paymentForProcessing);

        while (!processedPayments.isEmpty()) {
            UUID tmp = null;
            for (UUID code : processedPayments) {
                try {
                    queue.put(code);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                tmp = code;
                break;
            }
            processedPayments.remove(tmp);

            if (processedPayments.isEmpty()) {
                log.info("Check for appeared payments");
                haveNewPayments(processedPayments);
            }
        }
    }


    private List<UUID> paymentGateway(Collection<Payment> payments) {
        List<UUID> processedPaymentCode = new ArrayList<>();
        PaymentStatus[] statuses = PaymentStatus.values();

        for (Payment payment : payments) {
            payment.setStatus(statuses[new Random().nextInt(statuses.length)]);
            log.info("Payment with code " + payment.getPaymentCode() + " processed");
            paymentDAO.update(payment);

            processedPaymentCode.add(payment.getPaymentCode());
        }

        return processedPaymentCode;
    }

    private void haveNewPayments(List<UUID> codes) {
        List<Payment> appearedPayments = paymentDAO.getAllPaymentWithStatusNew();

        if (appearedPayments.isEmpty()) {
            log.info("New payments not found");
        } else {
            List<UUID> processedNewPayments = paymentGateway(appearedPayments);
            codes.addAll(processedNewPayments);
        }
    }
}
