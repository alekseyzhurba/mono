create table if not exists app_user (
full_name varchar(50) unique,
email varchar(20),
phone varchar(13) primary key);

create table if not exists cinema(
id int primary key,
name varchar(30) not null,
row_count int not null,
seats_in_row int not null);

create table if not exists movie(
id int primary key,
title varchar(255) not null
);

create table if not exists movie_session(
id int primary key,
movie_id int references movie (id),
cinema_id int REFERENCES cinema (id),
session_time timestamp,
price numeric(10,4));

create table if not exists ticket(
movie_session_id int references movie_session,
trow int not null ,
seat int not null,
user_login varchar(13) references app_user (phone),
status varchar(20),
created_at timestamp,
status_updated_at timestamp,
payment_code uuid,
primary key (movie_session_id, row, seat));

create table if not exists payment(
payment_code uuid not null,
amount numeric(8,4) not null,
card_num varchar(16) not null references app_user(full_name),
status varchar(15) not null);

-- Процедура проверки что ряд или место не больше чем кол-во рядов или мест в ряд в Cinema
CREATE procedure insert_data(
	movie_session_id int,
	trow smallint,
	seat smallint,
	user_login varchar(13),
	status varchar(20),
	created_at timestamp
	) language plpgsql as
$$
	declare
	row_in_ticket int;
	seat_in_ticket int;
	session_id int;
	rows_count int;
	seats_count int;
begin
row_in_ticket =  trow;
seat_in_ticket = seat;
session_id = movie_session_id;
select
    c.row_count,
    c.seats_in_row
into rows_count, seats_count
from
    cinema c
  join
    movie_session ms
  on ms.cinema_id  = c.id where ms.id = session_id;
	if (row_in_ticket > rows_count or seat_in_ticket > seats_count) then
		RAISE'seat or row incorrect';
		ROLLBACK;
	else
		insert into ticket (movie_session_id, trow, seat, user_login, status, created_at)
		values (movie_session_id, trow, seat, user_login, status, created_at);
	end if;
end;$$